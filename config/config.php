<?php

return [
	'endpoint' => env('REGIAPI_OAUTH2_ENDPOINT', 'http://regiweb.localhost/'),
	'endpoint_mock' => env('REGIAPI_OAUTH2_ENDPOINT_MOCK', 'http://localhost:4010'),
	'client_id' => env('REGIAPI_OAUTH2_CLIENT_ID'),
	'client_secret' => env('REGIAPI_OAUTH2_CLIENT_SECRET')
];
