<?php

namespace Adsysco\LaravelRegiCareApiClient\Providers;

use Adsysco\RegiCareApiClient\Rest\Client;
use Adsysco\LaravelRegiCareApiClient\LaravelClient;
use App;

class FakeClientServiceProvider extends ClientServiceProvider
{
    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__ . '/../../config/config.php', 'laravel-regicare-api-client');

        // Register the main class to use with the facade
        $this->app->singleton('laravel-regicare-fake-api-client', function () {
            return $this->getApiClient();
        });

        $this->app->singleton(LaravelClient::class, function () {
            return $this->getApiClient();
        });
    }
    
    private function getApiClient()
    {
        return new LaravelClient(Client::create($this->getEndpoint()));
    }

    protected function getEndpoint()
    {
        return config('laravel-regicare-api-client.endpoint_mock');
    }
}
