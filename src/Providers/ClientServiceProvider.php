<?php

namespace Adsysco\LaravelRegiCareApiClient\Providers;

use Illuminate\Support\ServiceProvider;
use Adsysco\RegiCareApiClient\Rest\Client;
use Adsysco\LaravelRegiCareApiClient\LaravelClient;
use App;

class ClientServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../../config/config.php' => config_path('laravel-regicare-api-client.php'),
            ], 'config');
        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__ . '/../../config/config.php', 'laravel-regicare-api-client');

        // Register the main class to use with the facade
        $this->app->singleton('laravel-regicare-api-client', function () {
            return $this->getApiClient();
        });

        $this->app->singleton(LaravelClient::class, function () {
            return $this->getApiClient();
        });
    }

    private function getApiClient()
    {
        return new LaravelClient(Client::create($this->getEndpoint()));
    }

    private function getEndpoint()
    {
        if (App::environment() === 'testing') {
            return config('laravel-regicare-api-client.endpoint_mock');
        }

        return config('laravel-regicare-api-client.endpoint');
    }
}
