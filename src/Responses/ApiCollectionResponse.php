<?php

namespace Adsysco\LaravelRegiCareApiClient\Responses;

use Illuminate\Support\Collection;

class ApiCollectionResponse extends Collection
{
    public function toArray()
    {
        return $this->map(function($item) {
           return $item->toArray();
        });
    }

    /**
     * Get the collection of items as JSON.
     *
     * @param  int  $options
     * @return string
     */
    public function toJson($options = 0)
    {
        return json_encode($this->toArray());
    }
}
