<?php

namespace Adsysco\LaravelRegiCareApiClient\Facades;

use Adsysco\RegiCareApiClient\Api\RegiCall;
use Adsysco\RegiCareApiClient\Api\RegiCall\Calls;
use Illuminate\Support\Facades\Facade;

/**
 * @see \Adsysco\LaravelRegiCareApiClient\LaravelClient
 *
 * @method static authenticateWithPasswordGrant(string $username, string $password)
 * @method static RegiCall regiCall()
 * @method static Calls call()
 */
class Client extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'laravel-regicare-api-client';
    }
}
