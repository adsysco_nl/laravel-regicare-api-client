<?php

namespace Adsysco\LaravelRegiCareApiClient;

use Adsysco\RegiCareApiClient\Rest\Client;
use Adsysco\RegiCareApiClient\Api\RegiCall;
use Adsysco\RegiCareApiClient\Api\RegiCall\Calls;
use Adsysco\RegiCareApiClient\Api\RegiCall\Periods;
use Adsysco\RegiCareApiClient\Client\Credentials\OAuthClientCredentials;
use Adsysco\RegiCareApiClient\Client\Credentials\OAuthPasswordGrantCredentials;

/**
 * Class LaravelRegiCareApiClient
 *
 * @method RegiCall regiCall()
 * @method Calls call()
 * @method Periods periods()
 *
 * @package Adsysco\LaravelRegiCareApiClient
 */
class LaravelClient
{
    /**
     * @var Client
     */
	private $client;

	private $isAuthenticated = false;

	public function __construct(Client $client)
	{
		$this->client = $client;
	}

    public function authenticateWithPasswordGrant($username, $password)
    {
        $credentials = new OAuthPasswordGrantCredentials(
            config('laravel-regicare-api-client.client_id'),
            config('laravel-regicare-api-client.client_secret'),
            $username,
            $password
        );

        $token = $this->client->authenticateWithPasswordGrant($credentials);

        session(['regicare-api-token' => $token]);
    }

    public function authenticateWithClientCredentialsGrant()
    {
        $credentials = new OAuthClientCredentials(
            config('laravel-regicare-api-client.client_id'),
            config('laravel-regicare-api-client.client_secret')
        );

        $token = $this->client->authenticateWithClientCredentialsGrant($credentials);

        session(['regicare-api-token-client' => $token]);
    }

    public function __call($method, $params)
	{
	    if (!$this->isAuthenticated && $this->tokenExists()) {
	        $this->setAuthenticatedClient();
        }

        return $this->client->$method($params);
	}

    private function tokenExists()
    {
        return session('regicare-api-token') !== null;
    }

    private function setAuthenticatedClient()
    {
        $token = session('regicare-api-token');
        $this->client = Client::withAuthenticationToken($token, $this->client->getEndpoint());
        $this->isAuthenticated = true;
        return $this;
    }
}
