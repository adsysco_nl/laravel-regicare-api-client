# Changelog

All notable changes to `laravel-regicare-api-client` will be documented in this file

## 1.0.1 - 2021-01-07

 - fixed regicare-api-client dependency

## 1.0.0 - 2021-01-07

- initial release
